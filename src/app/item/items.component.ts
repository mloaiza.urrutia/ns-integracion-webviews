import { Component, OnInit } from "@angular/core";
import { EnterFullscreenEventData, LoadEventData, LoadFinishedEventData, ShouldOverrideUrlLoadEventData, WebViewExt } from "@nota/nativescript-webview-ext";
import { listener, evento } from "./listener"

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html"
})
export class ItemsComponent implements OnInit {

    public webview: WebViewExt;

    constructor() { }

    ngOnInit(){
        
    }

    async webviewLoaded(args: LoadEventData) {
        this.webview = args.object;

        this.webview.on(WebViewExt.loadFinishedEvent, async (args: LoadFinishedEventData) => {
            console.log(`WebViewExt.loadFinishedEvent: ${args.url}`);
        });
    }

    async evento(){
        // const res = await this.webview.executeJavaScript("window.addEventListener('message', function(event) { console.log('eventlistener 1: '); console.log(event.data.url); return event.data; }); ", true);
        // this.webview.autoLoadJavaScriptFile("listener.js", "~/listener.js");
        let resp = await this.webview.executeJavaScript(evento, false);
        this.webview.executeJavaScript(evento)
        .then((value) => {
            // cumplimiento
            console.log("value: ");
            console.log(value);
           });
        console.log(resp);
    }
}
